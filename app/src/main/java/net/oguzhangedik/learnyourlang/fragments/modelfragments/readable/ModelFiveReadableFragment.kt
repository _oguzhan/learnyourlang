package net.oguzhangedik.learnyourlang.fragments.modelfragments.readable

import android.graphics.Color
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_readable_model_five.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.englishmodels.ModelFive
import net.oguzhangedik.learnyourlang.utils.Constants
import net.oguzhangedik.learnyourlang.utils.GlideApp

/**
 * Created by ordekci on 1.12.2017.
 */
class ModelFiveReadableFragment : ModelReadableFragment() {
    override fun getLayoutResourceId(): Int? {
        return R.layout.fragment_readable_model_five
    }

    lateinit var model: ModelFive

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = getIncomingModel<ModelFive>()
        tvEnglishWord.text = model.englishWord
        GlideApp.with(ivLeftImage.context).load(model.optionImageUrlOne).placeholder(R.drawable.placeholder).into(ivLeftImage)
        GlideApp.with(ivRightImage.context).load(model.optionImageUrlTwo).placeholder(R.drawable.placeholder).into(ivRightImage)
        tvDescription.text = model.information
    }

    override fun placeListeners() {
        super.placeListeners()
        flLeft.setOnClickListener { v -> checkSelection(v, flRight) }
        flRight.setOnClickListener { v -> checkSelection(v, flLeft) }
    }

    private fun checkSelection(clickedView: View, otherView: View) {
        val condition = model.trueOptionIndex == clickedView.tag.toString().toLong()
        showCheckingStateToast(Triple(condition, Constants.EMPTY_STRING, true))

        if (condition) clickedView.setBackgroundColor(Color.parseColor("#3300FF00"))
        else {
            otherView.setBackgroundColor(Color.parseColor("#3300FF00"))
            clickedView.setBackgroundColor(Color.parseColor("#33FF0000"))
            descriptionTitle.visibility = View.VISIBLE
            tvDescription.visibility = View.VISIBLE
        }
    }

    companion object {
        fun newInstance(): ModelFiveReadableFragment {
            return ModelFiveReadableFragment()
        }
    }
}