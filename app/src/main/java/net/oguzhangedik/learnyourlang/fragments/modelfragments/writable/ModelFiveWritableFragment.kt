package net.oguzhangedik.learnyourlang.fragments.modelfragments.writable

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_writable_model_five.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.englishmodels.ModelFive
import net.oguzhangedik.learnyourlang.firebase.FirebaseConstant
import net.oguzhangedik.learnyourlang.fragments.modelfragments.ModelWritableFragment
import net.oguzhangedik.learnyourlang.utils.Constants
import net.oguzhangedik.learnyourlang.utils.GlideApp
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import java.lang.Exception

/**
 * Created by ordekci on 7.12.2017.
 */
class ModelFiveWritableFragment : ModelWritableFragment() {
    lateinit var modelForUpdate: ModelFive
    override fun update() {
        context.toast("update five")
        if (ivFirstImage.tag != null && Constants.Tag.IMAGE_UPDATED == ivFirstImage.tag.toString()) {
            //delete current image url, then add new image and get url
            deleteSelectedImageFromFirestoreUsingUrl(ivFirstImage, modelForUpdate.optionImageUrlOne)
        } else if (ivSecondImage.tag != null && Constants.Tag.IMAGE_UPDATED == ivSecondImage.tag.toString()) {
            //delete current image url, then add new image and get url, then update model
            deleteSelectedImageFromFirestoreUsingUrl(ivSecondImage, modelForUpdate.optionImageUrlTwo)
        } else {
            //just update current model
            updateModel()
        }
    }

    private fun deleteSelectedImageFromFirestoreUsingUrl(ivSelectedImage: ImageView, selectedUrl: String) {
        if (ivSelectedImage.tag != null && Constants.Tag.IMAGE_UPDATED == ivSelectedImage.tag.toString()) {
            fun onComplete(task: Task<Void>) {
                if (!task.isSuccessful) context.toast("image not deleted ${task.exception.toString()}")
                /* image deletion isleminde silinme islemi basarili olmasa dahi, yeni gorselin upload
                   olmasi ve modelin guncellenme isleminin devam etmesi gerekir. */
                uploadUpdatedImage(ivSelectedImage)
            }
            deleteImageFromFirestorage(selectedUrl, { task -> onComplete(task) })
        }
    }

    private fun uploadUpdatedImage(ivSelectedImage: ImageView) {
        fun onFailure(exception: Exception) {
            showProgress(false)
            context.alert(exception.toString())
        }

        fun onSuccess(taskSnapshot: UploadTask.TaskSnapshot) {
            ivSelectedImage.tag = null
            if (ivSelectedImage.id == ivFirstImage.id)
                modelForUpdate.optionImageUrlOne = taskSnapshot.downloadUrl.toString()
            else if (ivSelectedImage.id == ivSecondImage.id)
                modelForUpdate.optionImageUrlTwo = taskSnapshot.downloadUrl.toString()
            update()
        }

        ivSelectedImage.isDrawingCacheEnabled = true
        ivSelectedImage.buildDrawingCache()
        uploadImage(ivSelectedImage.drawingCache, { exception -> onFailure(exception) }, { taskSnapshot -> onSuccess(taskSnapshot) })
    }

    private fun updateModel() {
        fun onComplete(task: Task<Void>) {
            showProgress(false)
            showCheckingStateToast(Triple(task.isSuccessful,
                    if (task.isSuccessful) "update successfull" else "update unsuccessfull", true))
        }
        modelForUpdate.englishWord = etEnteredEnglishWord.text.toString()
        modelForUpdate.information = etDescription.text.toString()
        modelForUpdate.trueOptionIndex = if (cbImageOne.isChecked) 0 else 1

        updateModel(FirebaseConstant.FireStore.Collections.EnglishModels, modelForUpdate, { task -> onComplete(task) })
    }

    val imageUrls: ArrayList<String> = ArrayList()
    lateinit var ivImage: ImageView

    override fun getEditTextsForNullCheck(): List<EditText>? {
        return listOf(etEnteredEnglishWord, etDescription)
    }

    override fun getLayoutResourceId(): Int? {
        return R.layout.fragment_writable_model_five
    }

    override fun save() {
        context.toast("Five")

        if (!(cbImageOne.isChecked || cbImageTwo.isChecked)) {
            context.toast("Checkbox lardan en az bir tanesi isaretli olmalidir.")
            return
        }

        fun onFailure(exception: Exception) {
            showProgress(false)
            context.alert(exception.toString())
        }

        fun onSuccess(taskSnapshot: UploadTask.TaskSnapshot) {
            imageUrls.add(taskSnapshot.downloadUrl.toString())
            if (imageUrls.size == 2)
                addNewModel(imageUrls[0], imageUrls[1])
        }

        ivImage.isDrawingCacheEnabled = true
        ivImage.buildDrawingCache()
        uploadImage(ivImage.drawingCache, { exception -> onFailure(exception) }, { taskSnapshot -> onSuccess(taskSnapshot) })

        listOf<ImageView>(ivFirstImage, ivSecondImage).forEach { it ->
            run {
                it.isDrawingCacheEnabled = true
                it.buildDrawingCache()
                uploadImage(it.drawingCache, { exception -> onFailure(exception) }, { taskSnapshot -> onSuccess(taskSnapshot) })
            }
        }
    }

    fun addNewModel(firstImageUrl: String, secondImageUrl: String) {
        fun onComplete(task: Task<DocumentReference>) {
            showProgress(false)
            showCheckingStateToast(Triple(task.isSuccessful, if (task.isSuccessful) "Model Basariyla eklendi." else Constants.EMPTY_STRING, true))
            if (!task.isSuccessful) context.alert("Model ekleme isleminde bir hata olustu ${task.exception.toString()}", "Uyari") {
                positiveButton("OK") {}
            }.show()
        }

        val model: ModelFive = ModelFive()
        model.englishWord = etEnteredEnglishWord.text.toString()
        model.optionImageUrlOne = firstImageUrl
        model.optionImageUrlTwo = secondImageUrl
        model.information = etDescription.text.toString()
        model.trueOptionIndex = if (cbImageOne.isChecked) 0 else 1

        addNewModel(FirebaseConstant.FireStore.Collections.EnglishModels, model, { task -> onComplete(task) })
    }

    override fun placeListeners() {
        super.placeListeners()
        ivFirstImage.setOnClickListener { v: View? -> optionImagesOnClick(v as ImageView) }
        ivSecondImage.setOnClickListener { v: View? -> optionImagesOnClick(v as ImageView) }
    }

    override fun onUpdateMode(model: Model) {
        super.onUpdateMode(model)
        modelForUpdate = model as ModelFive
        etEnteredEnglishWord.setText(model.englishWord)
        GlideApp.with(ivFirstImage.context).load(model.optionImageUrlOne).placeholder(R.drawable.placeholder).into(ivFirstImage)
        GlideApp.with(ivSecondImage.context).load(model.optionImageUrlTwo).placeholder(R.drawable.placeholder).into(ivSecondImage)
        etDescription.setText(model.information)
        cbImageOne.isChecked = model.trueOptionIndex == 0L
        cbImageTwo.isChecked = model.trueOptionIndex == 1L
    }

    private fun optionImagesOnClick(imageView: ImageView) {
        ivImage = imageView
        showImageSelectionAlert()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) return

        if (Constants.RequestCode.PICK_IMAGE == requestCode || Constants.RequestCode.CAMERA_REQUEST == requestCode) {
            val extras = data?.extras
            if (extras != null) {
                val bitmap = extras.getParcelable<Bitmap>("data")
                ivImage.setImageBitmap(bitmap)
                ivImage.tag = Constants.Tag.IMAGE_UPDATED
            } else context.toast("Resim alinamadi.")
        }
    }

    companion object {
        fun newInstance(): ModelFiveWritableFragment {
            return ModelFiveWritableFragment()
        }
    }
}