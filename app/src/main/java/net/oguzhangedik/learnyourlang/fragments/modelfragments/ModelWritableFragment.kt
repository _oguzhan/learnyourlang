package net.oguzhangedik.learnyourlang.fragments.modelfragments

import android.graphics.Bitmap
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.storage.UploadTask
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.firebase.CloudFireStore
import net.oguzhangedik.learnyourlang.firebase.CloudStorage
import net.oguzhangedik.learnyourlang.fragments.ModelReadableWritableCommonFragment
import net.oguzhangedik.learnyourlang.interfaces.Authorization
import net.oguzhangedik.learnyourlang.interfaces.IWritableFragmentUpdateMode
import net.oguzhangedik.learnyourlang.utils.CameraImageSelector
import net.oguzhangedik.learnyourlang.utils.Constants
import net.oguzhangedik.learnyourlang.utils.GalleryImageSelector
import net.oguzhangedik.learnyourlang.utils.PermissionUtil
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import java.lang.Exception

/**
 * Created by ordekci on 8.12.2017.
 */

abstract class ModelWritableFragment : ModelReadableWritableCommonFragment(), Authorization.FireStore.Index, Authorization.FireStorage.Upload, IWritableFragmentUpdateMode, Authorization.FireStore.Update, Authorization.FireStorage.Delete {
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null && arguments.containsKey(Constants.BundleKeys.MODEL)) {
            val modelForUpdate = arguments[Constants.BundleKeys.MODEL] as Model
            onUpdateMode(modelForUpdate)
        }
    }

    override fun Authorization.FireStore.Index.addNewModel(collection: String, model: Model, onComplete: ((task: Task<DocumentReference>) -> Unit)?, ignored0: (() -> Unit)?) {
        if (nullCheckIsSuccess()) {
            showProgress(true)

            CloudFireStore().addNewModel(collection, model, { task -> onComplete?.invoke(task) }) {
                showProgress(false)
                activity.toast("Check your network connection")
            }
        }
    }

    override fun Authorization.FireStorage.Upload.uploadImage(bitmap: Bitmap, onFailure: ((exception: Exception) -> Unit)?, onSuccess: ((taskSnapshot: UploadTask.TaskSnapshot) -> Unit)?, ignored0: (() -> Unit)?) {
        if (nullCheckIsSuccess()) {
            showProgress(true)
            CloudStorage().uploadImage(bitmap, { exception -> onFailure?.invoke(exception) }, { taskSnapshot -> onSuccess?.invoke(taskSnapshot) }) {
                showProgress(false)
                activity.toast("Check your network connection")
            }
        }
    }

    override fun Authorization.FireStorage.Delete.deleteImageFromFirestorage(imageUrl: String, onComplete: ((task: Task<Void>) -> Unit)?, throwConnectionError: (() -> Unit)?) {
        if (nullCheckIsSuccess()) {
            showProgress(true)
            CloudStorage().deleteImageFromFirestorage(imageUrl, { task -> onComplete?.invoke(task) }) {
                showProgress(false)
                activity.toast("Check your network connection")
            }
        }
    }

    override fun Authorization.FireStore.Update.updateModel(collection: String, model: Model, onComplete: ((task: Task<Void>) -> Unit)?, throwConnectionError: (() -> Unit)?) {
        if (nullCheckIsSuccess()) {
            showProgress(true)
            CloudFireStore().updateModel(collection, model, { task -> onComplete?.invoke(task) }) {
                showProgress(false)
                activity.toast("Check your network connection")
            }
        }
    }

    private fun nullCheckIsSuccess(): Boolean {
        if (getEditTextsForNullCheck()?.isNotEmpty()!!) {
            val emptyEditTexts: List<EditText>? = getEditTextsForNullCheck()?.filter { et -> TextUtils.isEmpty(et.text.toString()) }

            if (emptyEditTexts?.size!! > 0) {
                val focusedView = emptyEditTexts[0]
                focusedView.error = getString(R.string.error_field_required)
                focusedView.requestFocus()
                return false
            }
        }
        return true
    }

    protected fun showImageSelectionAlert() =
            context.alert("Select an image or capture from", "Hi Dude!") {
                positiveButton("Gallery") {
                    openGallery()
                }
                negativeButton("Camera") {
                    if (PermissionUtil.checkCamera(this@ModelWritableFragment)) {
                        openCamera()
                    }
                }
            }.show()

    protected fun openGallery() =
            GalleryImageSelector.pickImage(this@ModelWritableFragment, Constants.RequestCode.PICK_IMAGE)


    protected fun openCamera() =
            CameraImageSelector.captureImage(this, Constants.RequestCode.CAMERA_REQUEST)


    abstract fun getEditTextsForNullCheck(): List<EditText>?
    abstract fun save()
    abstract fun update()

    override fun onUpdateMode(model: Model) = Unit
}