package net.oguzhangedik.learnyourlang.fragments.modelfragments.readable

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_readable_model_one.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.englishmodels.ModelOne
import net.oguzhangedik.learnyourlang.utils.GlideApp

/**
 * Created by ordekci on 1.12.2017.
 */
class ModelOneReadableFragment : ModelReadableFragment() {
    override fun getLayoutResourceId(): Int? {
        return R.layout.fragment_readable_model_one
    }

    lateinit var model: ModelOne

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = getIncomingModel<ModelOne>()
        tvEnglishWord.text = model.englishWord
        GlideApp.with(ivImage.context).load(model.imageUrl).placeholder(R.drawable.placeholder).into(ivImage)
        tvTurkishWord.text = model.turkishWord
        tvDescription.text = model.information
        tvSampleEnglishSentence.text = model.sampleEnglishSentence
        tvSampleTurkishSentence.text = model.sampleTurkishSentence
    }

    companion object {
        fun newInstance(): ModelOneReadableFragment {
            return ModelOneReadableFragment()
        }
    }
}