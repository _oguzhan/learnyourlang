package net.oguzhangedik.learnyourlang.fragments.modelfragments.readable

import android.os.Bundle
import android.view.View
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.fragments.ModelReadableWritableCommonFragment
import net.oguzhangedik.learnyourlang.utils.Constants

/**
 * Created by ordekci on 31.12.2017.
 */
abstract class ModelReadableFragment : ModelReadableWritableCommonFragment() {
    lateinit var tempModel: Model

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tempModel = arguments[Constants.BundleKeys.MODEL] as Model
    }

    inline fun <reified T : Model> getIncomingModel(): T {
        return tempModel as T
    }
}