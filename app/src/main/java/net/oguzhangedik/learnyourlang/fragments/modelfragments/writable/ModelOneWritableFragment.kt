package net.oguzhangedik.learnyourlang.fragments.modelfragments.writable

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.widget.EditText
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_writable_model_one.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.englishmodels.ModelOne
import net.oguzhangedik.learnyourlang.firebase.FirebaseConstant
import net.oguzhangedik.learnyourlang.fragments.modelfragments.ModelWritableFragment
import net.oguzhangedik.learnyourlang.utils.Constants
import net.oguzhangedik.learnyourlang.utils.GlideApp
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import java.lang.Exception

/**
 * Created by ordekci on 7.12.2017.
 */
class ModelOneWritableFragment : ModelWritableFragment() {
    lateinit var modelForUpdate: ModelOne
    override fun update() {
        if (ivImage.tag != null && Constants.Tag.IMAGE_UPDATED == ivImage.tag.toString()) {
            //delete current image url, then add new image and get url, then update model
            fun onComplete(task: Task<Void>) {
                if (!task.isSuccessful) context.toast("image not deleted ${task.exception.toString()}")
                /* image deletion isleminde silinme islemi basarili olmasa dahi, yeni gorselin upload
                  olmasi ve modelin guncellenme isleminin devam etmesi gerekir. */
                uploadUpdatedImage()
            }
            deleteImageFromFirestorage(modelForUpdate.imageUrl, { task -> onComplete(task) })
        } else {
            //just update current model
            updateModel()
        }
    }

    private fun uploadUpdatedImage() {
        fun onFailure(exception: Exception) {
            showProgress(false)
            context.alert(exception.toString())
        }

        fun onSuccess(taskSnapshot: UploadTask.TaskSnapshot) = updateModel(taskSnapshot.downloadUrl.toString())

        ivImage.isDrawingCacheEnabled = true
        ivImage.buildDrawingCache()
        uploadImage(ivImage.drawingCache, { exception -> onFailure(exception) }, { taskSnapshot -> onSuccess(taskSnapshot) })
    }

    private fun updateModel(imageUrl: String? = null) {
        if (imageUrl != null) modelForUpdate.imageUrl = imageUrl
        fun onComplete(task: Task<Void>) {
            showProgress(false)
            ivImage.tag = null
            showCheckingStateToast(Triple(task.isSuccessful, if (task.isSuccessful) "update successfull" else "update unsuccessfull", true))
        }
        modelForUpdate.englishWord = etEnteredEnglishWord.text.toString()
        //modelForUpdate.imageUrl = imageUrl
        modelForUpdate.turkishWord = etEnteredTurkishWord.text.toString()
        modelForUpdate.information = etDescription.text.toString()
        modelForUpdate.sampleEnglishSentence = etSampleEnglishSentence.text.toString()
        modelForUpdate.sampleTurkishSentence = etSampleTurkishSentence.text.toString()

        updateModel(FirebaseConstant.FireStore.Collections.EnglishModels, modelForUpdate, { task -> onComplete(task) })
    }

    override fun getEditTextsForNullCheck(): List<EditText>? {
        return listOf(etEnteredEnglishWord, etEnteredTurkishWord, etDescription, etSampleEnglishSentence, etSampleTurkishSentence)
    }

    override fun getLayoutResourceId(): Int? {
        return R.layout.fragment_writable_model_one
    }

    override fun save() {
        context.toast("One")

        fun onFailure(exception: Exception) {
            showProgress(false)
            context.alert(exception.toString())
        }

        fun onSuccess(taskSnapshot: UploadTask.TaskSnapshot) = addNewModel(taskSnapshot.downloadUrl.toString())

        ivImage.isDrawingCacheEnabled = true
        ivImage.buildDrawingCache()
        uploadImage(ivImage.drawingCache, { exception -> onFailure(exception) }, { taskSnapshot -> onSuccess(taskSnapshot) })
    }

    fun addNewModel(imageUrl: String) {

        fun onComplete(task: Task<DocumentReference>) {
            showProgress(false)
            showCheckingStateToast(Triple(task.isSuccessful, if (task.isSuccessful) "Model Basariyla eklendi." else Constants.EMPTY_STRING, true))
            if (!task.isSuccessful) context.alert("Model ekleme isleminde bir hata olustu ${task.exception.toString()}", "Uyari") {
                positiveButton("OK") {}
            }.show()
        }

        val model: ModelOne = ModelOne()
        model.englishWord = etEnteredEnglishWord.text.toString()
        model.imageUrl = imageUrl
        model.turkishWord = etEnteredTurkishWord.text.toString()
        model.information = etDescription.text.toString()
        model.sampleEnglishSentence = etSampleEnglishSentence.text.toString()
        model.sampleTurkishSentence = etSampleTurkishSentence.text.toString()

        addNewModel(FirebaseConstant.FireStore.Collections.EnglishModels, model, { task -> onComplete(task) })
    }

    override fun placeListeners() {
        super.placeListeners()
        ivImage.setOnClickListener {
            showImageSelectionAlert()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constants.RequestCode.PERMISSION_REQUEST_CAMERA -> if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_OK) return

        val extras = data?.extras
        if (extras != null) {
            if (Constants.RequestCode.PICK_IMAGE == requestCode || Constants.RequestCode.CAMERA_REQUEST == requestCode) {
                val bitmap = extras.getParcelable<Bitmap>("data")
                ivImage.setImageBitmap(bitmap)
                ivImage.tag = Constants.Tag.IMAGE_UPDATED
            } else context.toast("Resim alinamadi.")
        }
    }

    override fun onUpdateMode(model: Model) {
        super.onUpdateMode(model)
        modelForUpdate = model as ModelOne
        etEnteredEnglishWord.setText(model.englishWord)
        GlideApp.with(ivImage.context).load(model.imageUrl).placeholder(R.drawable.placeholder).into(ivImage)
        etEnteredTurkishWord.setText(model.turkishWord)
        etDescription.setText(model.information)
        etSampleEnglishSentence.setText(model.sampleEnglishSentence)
        etSampleTurkishSentence.setText(model.sampleTurkishSentence)
    }

    companion object {
        fun newInstance(): ModelOneWritableFragment {
            return ModelOneWritableFragment()
        }
    }
}