package net.oguzhangedik.learnyourlang.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.oguzhangedik.learnyourlang.activities.BaseAppCompatActivity
import net.oguzhangedik.learnyourlang.interfaces.ILayoutResource
import net.oguzhangedik.learnyourlang.interfaces.IPlaceListeners
import net.oguzhangedik.learnyourlang.interfaces.IProgressAccess

/**
 * Created by ordekci on 8.12.2017.
 */
abstract class BaseFragment : Fragment(), ILayoutResource, IProgressAccess, IPlaceListeners {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(getLayoutResourceId()!!, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        placeListeners()
    }

    override fun showProgress(show: Boolean) {
        if(activity == null) return

        if (activity is BaseAppCompatActivity)
            (activity as BaseAppCompatActivity).showProgress(show)
        else
            throw UnsupportedOperationException("You can not call showProgress if activity is not BaseAppCompatActivity")
    }

    override fun placeListeners() = Unit
}