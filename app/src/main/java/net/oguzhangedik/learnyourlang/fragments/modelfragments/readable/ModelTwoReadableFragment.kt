package net.oguzhangedik.learnyourlang.fragments.modelfragments.readable

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_readable_model_two.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.englishmodels.ModelTwo

/**
 * Created by oguz on 1.12.2017.
 */
class ModelTwoReadableFragment : ModelReadableFragment() {
    override fun getLayoutResourceId(): Int? {
        return R.layout.fragment_readable_model_two
    }

    lateinit var model: ModelTwo

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = getIncomingModel<ModelTwo>()
        tvSampleEnglishSentence.text = model.sampleEnglishSentence
        tvSampleTurkishSentence.text = model.sampleTurkishSentence
    }

    companion object {
        fun newInstance(): ModelTwoReadableFragment {
            return ModelTwoReadableFragment()
        }
    }
}