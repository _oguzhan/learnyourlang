package net.oguzhangedik.learnyourlang.fragments.modelfragments.writable

import android.widget.EditText
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import kotlinx.android.synthetic.main.fragment_writable_model_three.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.englishmodels.ModelThree
import net.oguzhangedik.learnyourlang.firebase.FirebaseConstant
import net.oguzhangedik.learnyourlang.fragments.modelfragments.ModelWritableFragment
import net.oguzhangedik.learnyourlang.utils.Constants
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

/**
 * Created by ordekci on 7.12.2017.
 */
class ModelThreeWritableFragment : ModelWritableFragment() {
    lateinit var modelForUpdate: ModelThree

    override fun update() {
        context.toast("update three")

        fun onComplete(task: Task<Void>) {
            showProgress(false)
            showCheckingStateToast(Triple(task.isSuccessful, if (task.isSuccessful) "update successfull" else "update unsuccessfull", true))
        }
        modelForUpdate.information = etInformation.text.toString()
        modelForUpdate.sampleEnglishSentence = etSampleEnglishSentence.text.toString()
        modelForUpdate.sampleTurkishSentence = etSampleTurkishSentence.text.toString()

        updateModel(FirebaseConstant.FireStore.Collections.EnglishModels, modelForUpdate, { task -> onComplete(task) })
    }

    override fun getEditTextsForNullCheck(): List<EditText>? {
        return listOf(etInformation, etSampleEnglishSentence, etSampleTurkishSentence)
    }

    override fun getLayoutResourceId(): Int? {
        return R.layout.fragment_writable_model_three
    }

    override fun save() {
        context.toast("Three")

        fun onComplete(task: Task<DocumentReference>) {
            showProgress(false)
            showCheckingStateToast(Triple(task.isSuccessful, if (task.isSuccessful) "Model Basariyla eklendi." else Constants.EMPTY_STRING, true))
            if (!task.isSuccessful) context.alert("Model ekleme isleminde bir hata olustu ${task.exception.toString()}", "Uyari") {
                positiveButton("OK") {}
            }.show()
        }

        val model: ModelThree = ModelThree()
        model.information = etInformation.text.toString()
        model.sampleEnglishSentence = etSampleEnglishSentence.text.toString()
        model.sampleTurkishSentence = etSampleTurkishSentence.text.toString()

        addNewModel(FirebaseConstant.FireStore.Collections.EnglishModels, model, { task -> onComplete(task) })
    }

    override fun onUpdateMode(model: Model) {
        super.onUpdateMode(model)
        modelForUpdate = model as ModelThree
        etInformation.setText(model.information)
        etSampleEnglishSentence.setText(model.sampleEnglishSentence)
        etSampleTurkishSentence.setText(model.sampleTurkishSentence)
    }

    companion object {
        fun newInstance(): ModelThreeWritableFragment {
            return ModelThreeWritableFragment()
        }
    }
}