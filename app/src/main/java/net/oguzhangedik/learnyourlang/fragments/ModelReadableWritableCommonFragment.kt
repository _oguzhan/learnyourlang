package net.oguzhangedik.learnyourlang.fragments

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.utils.Constants
import org.jetbrains.anko.toast

/**
 * Created by ordekci on 7.01.2018.
 */
abstract class ModelReadableWritableCommonFragment : BaseFragment() {
    private var toast: Toast? = null
    /** isOkay, message,isCustomOrNormalToast */
    private var tripleToast: Triple<Boolean, String, Boolean>? = null
    private var toastHandler: Handler = Handler()
    @SuppressLint("LongLogTag")
    private var runnableOfToast = Runnable {
        //Ref : https://stackoverflow.com/a/28673131/3341089
        if (activity == null) Log.w("ModelReadableWritableCommonFragment","activity is null, " +
                "this may be about fragment detaching. like ''java.lang.IllegalStateException: " +
                "onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.''")
         else if (tripleToast?.third!!) {
            val layout = layoutInflater.inflate(R.layout.toast_check, null)
            layout.setBackgroundColor(Color.parseColor(if (tripleToast?.first!!/*isOkay*/) "#5500AA00" else "#55AA0000"))
            layout.findViewById<ImageView>(R.id.ivCheckState)
                    .setImageResource(if (tripleToast?.first!!/*isOkay*/) R.drawable.okay_icon else R.drawable.not_okay_icon)

            val tvMessage = layout.findViewById<TextView>(R.id.tvMessage)
            tvMessage.visibility = if(tripleToast?.second == null || tripleToast?.second!!.trim() == Constants.EMPTY_STRING) View.GONE else View.VISIBLE
            tvMessage.text = tripleToast?.second

            if (toast != null) toast?.cancel()

            toast = Toast(context)
            toast?.setGravity(Gravity.FILL, 0, 0)
            toast?.duration = Toast.LENGTH_SHORT
            toast?.view = layout
            toast?.show()
        } else context.toast(tripleToast?.second!!)
    }

    /**
     * tripleToast.first = isOkay
     * tripleToast.second = message
     * tripleToast.third = isCustomOrNormalToast
     */
    protected fun showCheckingStateToast(tripleToast: Triple<Boolean, String, Boolean>) {
        this.tripleToast = tripleToast

        toastHandler.post(runnableOfToast)
    }

    override fun onDetach() {
        super.onDetach()
        toastHandler.removeCallbacks(runnableOfToast)
    }

    override fun onPause() {
        super.onPause()
        toast?.cancel()
    }

    /* Ref : https://stackoverflow.com/a/41894595/3341089
     If Viewpager has swiped and we need to know that current fragment is visiple to user or not,
     we can handle this stuation via setUserVisibleHint callback method.
    */
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (!isVisibleToUser && toast != null) toast?.cancel()
    }
}