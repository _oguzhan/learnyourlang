package net.oguzhangedik.learnyourlang.fragments.modelfragments.readable

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_readable_model_three.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.englishmodels.ModelThree


/**
 * Created by ordekci on 1.12.2017.
 */
class ModelThreeReadableFragment : ModelReadableFragment() {
    override fun getLayoutResourceId(): Int? {
        return R.layout.fragment_readable_model_three
    }

    lateinit var model: ModelThree

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = getIncomingModel<ModelThree>()
        tvDescription.text = model.information
        tvSampleEnglishSentence.text = model.sampleEnglishSentence
        tvSampleTurkishSentence.text = model.sampleTurkishSentence

        if (model.sampleEnglishSentence.trim() == "") {
            sampleEnglishSentenceTitle.visibility = View.GONE
            tvSampleEnglishSentence.visibility = View.GONE
        }
        if (model.sampleTurkishSentence.trim() == "") {
            sampleTurkishSentenceTitle.visibility = View.GONE
            tvSampleTurkishSentence.visibility = View.GONE
        }
    }

    companion object {
        fun newInstance(): ModelThreeReadableFragment {
            return ModelThreeReadableFragment()
        }
    }
}