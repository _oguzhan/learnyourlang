package net.oguzhangedik.learnyourlang.fragments.modelfragments.readable

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_readable_model_four.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.englishmodels.ModelFour
import net.oguzhangedik.learnyourlang.utils.Constants
import net.oguzhangedik.learnyourlang.utils.GlideApp

/**
 * Created by ordekci on 1.12.2017.
 */
class ModelFourReadableFragment : ModelReadableFragment() {
    override fun getLayoutResourceId(): Int? {
        return R.layout.fragment_readable_model_four
    }

    lateinit var model: ModelFour

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = getIncomingModel<ModelFour>()
        btnWordOne.text = model.optionTextOne
        btnTwo.text = model.optionTextTwo
        tvDescription.text = model.information
        GlideApp.with(ivImage.context).load(model.imageUrl).placeholder(R.drawable.placeholder).into(ivImage)
    }

    override fun placeListeners() {
        super.placeListeners()
        btnWordOne.setOnClickListener { v -> checkSelection(v, btnTwo) }
        btnTwo.setOnClickListener { v -> checkSelection(v, btnWordOne) }
    }


    private fun checkSelection(clickedView: View, otherView: View) {
        val condition = model.trueOptionIndex == clickedView.tag.toString().toLong()
        showCheckingStateToast(Triple(condition, Constants.EMPTY_STRING, true))

        if (condition) clickedView.setBackgroundResource(R.drawable.button_green_bg)
        else {
            otherView.setBackgroundResource(R.drawable.button_green_bg)
            clickedView.setBackgroundResource(R.drawable.button_red_bg)
            descriptionTitle.visibility = View.VISIBLE
            tvDescription.visibility = View.VISIBLE
        }
    }

    companion object {
        fun newInstance(): ModelFourReadableFragment {
            return ModelFourReadableFragment()
        }
    }
}