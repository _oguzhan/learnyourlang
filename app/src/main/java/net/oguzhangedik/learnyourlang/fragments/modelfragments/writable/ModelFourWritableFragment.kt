package net.oguzhangedik.learnyourlang.fragments.modelfragments.writable

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.widget.EditText
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_writable_model_four.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.englishmodels.ModelFour
import net.oguzhangedik.learnyourlang.firebase.FirebaseConstant
import net.oguzhangedik.learnyourlang.fragments.modelfragments.ModelWritableFragment
import net.oguzhangedik.learnyourlang.utils.Constants
import net.oguzhangedik.learnyourlang.utils.GlideApp
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import java.lang.Exception

/**
 * Created by ordekci on 7.12.2017.
 */
class ModelFourWritableFragment : ModelWritableFragment() {
    lateinit var modelForUpdate: ModelFour

    override fun update() {
        context.toast("update four")
        if (ivImage.tag != null && Constants.Tag.IMAGE_UPDATED == ivImage.tag.toString()) {
            //delete current image url, then add new image and get url, then update model
            fun onComplete(task: Task<Void>) {
                if (!task.isSuccessful) context.toast("image not deleted ${task.exception.toString()}")
                /* image deletion isleminde silinme islemi basarili olmasa dahi, yeni gorselin upload
                   olmasi ve modelin guncellenme isleminin devam etmesi gerekir. */
                uploadUpdatedImage()
            }
            deleteImageFromFirestorage(modelForUpdate.imageUrl, { task -> onComplete(task) })
        } else {
            //just update current model
            updateModel()
        }
    }

    private fun uploadUpdatedImage() {
        fun onFailure(exception: Exception) {
            showProgress(false)
            context.alert(exception.toString())
        }

        fun onSuccess(taskSnapshot: UploadTask.TaskSnapshot) = updateModel(taskSnapshot.downloadUrl.toString())

        ivImage.isDrawingCacheEnabled = true
        ivImage.buildDrawingCache()
        uploadImage(ivImage.drawingCache, { exception -> onFailure(exception) }, { taskSnapshot -> onSuccess(taskSnapshot) })
    }

    private fun updateModel(imageUrl: String? = null) {
        if (imageUrl != null) modelForUpdate.imageUrl = imageUrl
        fun onComplete(task: Task<Void>) {
            showProgress(false)
            ivImage.tag = null
            showCheckingStateToast(Triple(task.isSuccessful,
                    if (task.isSuccessful) "update successfull" else "update unsuccessfull", true))
        }

        modelForUpdate.optionTextOne = etWordOne.text.toString()
        modelForUpdate.optionTextTwo = etWordTwo.text.toString()
        modelForUpdate.information = etDescription.text.toString()
        modelForUpdate.trueOptionIndex = if (cbWordOne.isChecked) 0 else 1

        updateModel(FirebaseConstant.FireStore.Collections.EnglishModels, modelForUpdate, { task -> onComplete(task) })
    }

    override fun getEditTextsForNullCheck(): List<EditText>? {
        return listOf(etWordOne, etWordTwo, etDescription)
    }

    override fun getLayoutResourceId(): Int? {
        return R.layout.fragment_writable_model_four
    }

    override fun save() {
        context.toast("Four")

        if (!(cbWordOne.isChecked || cbWordTwo.isChecked)) {
            context.toast("Checkbox lardan en az bir tanesi isaretli olmalidir.")
            return
        }

        fun onFailure(exception: Exception) {
            showProgress(false)
            context.alert(exception.toString())
        }

        fun onSuccess(taskSnapshot: UploadTask.TaskSnapshot) = addNewModel(taskSnapshot.downloadUrl.toString())

        ivImage.isDrawingCacheEnabled = true
        ivImage.buildDrawingCache()
        uploadImage(ivImage.drawingCache, { exception -> onFailure(exception) }, { taskSnapshot -> onSuccess(taskSnapshot) })
    }

    fun addNewModel(imageUrl: String) {
        fun onComplete(task: Task<DocumentReference>) {
            showProgress(false)
            showCheckingStateToast(Triple(task.isSuccessful, if (task.isSuccessful) "Model Basariyla eklendi." else Constants.EMPTY_STRING, true))
            if (!task.isSuccessful) else context.alert("Model ekleme isleminde bir hata olustu ${task.exception.toString()}", "Uyari") {
                positiveButton("OK") {}
            }.show()
        }

        val model: ModelFour = ModelFour()
        model.imageUrl = imageUrl
        model.optionTextOne = etWordOne.text.toString()
        model.optionTextTwo = etWordTwo.text.toString()
        model.information = etDescription.text.toString()
        model.trueOptionIndex = if (cbWordOne.isChecked) 0 else 1
        addNewModel(FirebaseConstant.FireStore.Collections.EnglishModels, model, { task -> onComplete(task) })
    }

    override fun placeListeners() {
        super.placeListeners()
        ivImage.setOnClickListener {
            showImageSelectionAlert()
        }
    }

    override fun onUpdateMode(model: Model) {
        super.onUpdateMode(model)
        modelForUpdate = model as ModelFour
        GlideApp.with(ivImage.context).load(model.imageUrl).placeholder(R.drawable.placeholder).into(ivImage)
        etWordOne.setText(model.optionTextOne)
        etWordTwo.setText(model.optionTextTwo)
        etDescription.setText(model.information)
        cbWordOne.isChecked = model.trueOptionIndex == 0L
        cbWordTwo.isChecked = model.trueOptionIndex == 1L
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) return

        val extras = data?.extras
        if (Constants.RequestCode.PICK_IMAGE == requestCode || Constants.RequestCode.CAMERA_REQUEST == requestCode) {
            if (extras != null) {
                val bitmap = extras.getParcelable<Bitmap>("data")
                ivImage.setImageBitmap(bitmap)
                ivImage.tag = Constants.Tag.IMAGE_UPDATED
            } else context.toast("Resim alinamadi.")
        }
    }

    companion object {
        fun newInstance(): ModelFourWritableFragment {
            return ModelFourWritableFragment()
        }
    }
}