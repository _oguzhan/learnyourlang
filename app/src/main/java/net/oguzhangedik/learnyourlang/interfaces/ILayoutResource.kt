package net.oguzhangedik.learnyourlang.interfaces

/**
 * Created by ordekci on 8.12.2017.
 */
interface ILayoutResource {
    /*In activities, if return null value, that step that this using this ignores*/
    fun getLayoutResourceId(): Int?
}