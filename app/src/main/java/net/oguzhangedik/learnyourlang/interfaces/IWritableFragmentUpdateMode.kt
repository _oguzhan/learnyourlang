package net.oguzhangedik.learnyourlang.interfaces

import net.oguzhangedik.learnyourlang.abstracts.Model

/**
 * Created by admin on 19.12.2017.
 */
interface IWritableFragmentUpdateMode {
    fun onUpdateMode(model : Model)
}