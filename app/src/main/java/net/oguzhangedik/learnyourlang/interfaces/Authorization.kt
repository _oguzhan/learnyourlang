package net.oguzhangedik.learnyourlang.interfaces

import android.content.Context
import android.graphics.Bitmap
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ProviderQueryResult
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import net.oguzhangedik.learnyourlang.abstracts.Model
import java.lang.Exception

/**
 * Created by ordekci on 16.11.2017.
 */
interface Authorization {

    interface ApplicationContextAccess {
        fun getApplicationContext(context: Context)
    }

    interface Firebase : Authorization {
        fun getFirebaseAuth(mAuth: FirebaseAuth?)

    }

    interface FireStore : Authorization {
        fun getFireStore(fireStore: FirebaseFirestore)

        interface Get : Authorization {
            fun Authorization.FireStore.Get.getAll(collection: String, onComplete: ((task: Task<QuerySnapshot>) -> Unit)? = null, throwConnectionError: (() -> Unit)? = null)
        }

        interface Index : Authorization {
            fun Authorization.FireStore.Index.addNewModel(collection: String, model: Model, onComplete: ((task: Task<DocumentReference>) -> Unit)? = null, throwConnectionError: (() -> Unit)? = null)
        }

        interface Update : Authorization {
            fun Authorization.FireStore.Update.updateModel(collection: String, model: Model, onComplete: ((task: Task<Void>) -> Unit)? = null, throwConnectionError: (() -> Unit)? = null)
        }
    }

    interface FireStorage : Authorization {
        fun getFireStorage(firebaseStorage: FirebaseStorage)

        interface Upload : Authorization {
            fun Authorization.FireStorage.Upload.uploadImage(bitmap: Bitmap, onFailure: ((exception: Exception) -> Unit)? = null, onSuccess: ((taskSnapshot: UploadTask.TaskSnapshot) -> Unit)? = null, throwConnectionError: (() -> Unit)? = null)
        }

        interface Delete : Authorization {
            fun Authorization.FireStorage.Delete.deleteImageFromFirestorage(imageUrl: String, onComplete: ((task: Task<Void>) -> Unit)? = null, throwConnectionError: (() -> Unit)? = null)
        }
    }

    interface SignIn : Authorization {
        fun Authorization.SignIn.signIn(email: String, password: String, onComplete: ((task: Task<AuthResult>) -> Unit)? = null, throwConnectionError: (() -> Unit)? = null)
    }

    interface Register : Authorization {
        fun Authorization.Register.register(email: String, password: String, onComplete: ((task: Task<AuthResult>) -> Unit)? = null, throwConnectionError: (() -> Unit)? = null)
        fun Authorization.Register.isUserRegistered(email: String, onComplete: ((task: Task<ProviderQueryResult>) -> Unit)? = null, throwConnectionError: (() -> Unit)? = null)
    }
}