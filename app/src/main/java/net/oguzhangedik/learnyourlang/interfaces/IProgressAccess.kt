package net.oguzhangedik.learnyourlang.interfaces

/**
 * Created by admin on 11.12.2017.
 */
interface IProgressAccess {
    fun showProgress(show: Boolean)
}