package net.oguzhangedik.learnyourlang.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import net.oguzhangedik.learnyourlang.fragments.modelfragments.ModelWritableFragment
import net.oguzhangedik.learnyourlang.fragments.modelfragments.writable.*

/**
 * Created by admin on 8.12.2017.
 */
class WritableModelPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    val writableFragments: List<ModelWritableFragment> =
            listOf(ModelOneWritableFragment.newInstance(),
                    ModelTwoWritableFragment.newInstance(),
                    ModelThreeWritableFragment.newInstance(),
                    ModelFourWritableFragment.newInstance(),
                    ModelFiveWritableFragment.newInstance()
            )

    override fun getItem(position: Int): ModelWritableFragment {
        return writableFragments[position]
    }

    override fun getCount(): Int {
        return writableFragments.size
    }

    fun getCurrentFragment(index: Int): ModelWritableFragment =  writableFragments[index]
}