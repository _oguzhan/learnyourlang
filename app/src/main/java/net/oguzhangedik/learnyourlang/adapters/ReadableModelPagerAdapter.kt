package net.oguzhangedik.learnyourlang.adapters

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.englishmodels.*
import net.oguzhangedik.learnyourlang.fragments.modelfragments.readable.*
import net.oguzhangedik.learnyourlang.utils.Constants

/**
 * Created by ordekci on 1.12.2017.
 */
class ReadableModelPagerAdapter(fragmentManager: FragmentManager, var models: ArrayList<Model>) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        val model = models[position]

        val fragment = when (model) {
            is ModelOne -> ModelOneReadableFragment.newInstance()
            is ModelTwo -> ModelTwoReadableFragment.newInstance()
            is ModelThree -> ModelThreeReadableFragment.newInstance()
            is ModelFour -> ModelFourReadableFragment.newInstance()
            is ModelFive -> ModelFiveReadableFragment.newInstance()
            else -> throw CloneNotSupportedException("You must add your new model check in ReadableModelPagerAdapter -- getItem")
        }

        val bundle = Bundle()
        bundle.putSerializable(Constants.BundleKeys.MODEL, model)
        fragment.arguments = bundle

        return fragment
    }

    override fun getCount(): Int {
        return models.size
    }

    fun getCurrentModel(index : Int) : Model = models[index]
}