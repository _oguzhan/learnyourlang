package net.oguzhangedik.learnyourlang

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import net.oguzhangedik.learnyourlang.interfaces.Authorization

/**
 * Created by ordekci on 16.11.2017.
 */
class LearnYourLangApplication : MultiDexApplication() {

    init {
        instance = this
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {
        private lateinit var instance: LearnYourLangApplication

        fun applicationContext(): Context {
            return instance.applicationContext
        }

        fun takeAuthorityToUseFirebaseAuthentication(candidate: Authorization) {
            when (candidate) {
                is Authorization.Firebase -> candidate.getFirebaseAuth(FirebaseAuth.getInstance())
            }
        }

        fun takeAuthorityToUseApplicationContext(candidate: Authorization) {
            when (candidate) {
                is Authorization.ApplicationContextAccess -> candidate.getApplicationContext(LearnYourLangApplication.applicationContext())
            }
        }

        fun takeAuthorityToUseFirebaseFirestore(candidate: Authorization) {
            when (candidate) {
                is Authorization.FireStore -> candidate.getFireStore(FirebaseFirestore.getInstance())
            }
        }

        fun takeAuthorityToUseFirebaseStorage(candidate: Authorization) {
            when (candidate) {
                is Authorization.FireStorage -> candidate.getFireStorage(FirebaseStorage.getInstance())
            }
        }

    }
}