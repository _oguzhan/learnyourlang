package net.oguzhangedik.learnyourlang.abstracts

import com.google.firebase.auth.FirebaseAuth
import net.oguzhangedik.learnyourlang.LearnYourLangApplication
import net.oguzhangedik.learnyourlang.interfaces.Authorization
import java.io.Serializable
import java.util.*

/**
 * Created by ordekci on 28.11.2017.
 */
abstract class Model : Authorization.Firebase, Serializable {
    var id: String = "" //id degeri sonradan eklenecek
    var _type: String = this::class.simpleName!!
    var isPublic = false
    var millis = Calendar.getInstance(TimeZone.getTimeZone("UTC")).timeInMillis
    lateinit var userId: String

    override fun getFirebaseAuth(mAuth: FirebaseAuth?) {
        userId = mAuth?.currentUser?.uid!!
    }

    fun updateMillis() {
        millis = Calendar.getInstance(TimeZone.getTimeZone("UTC")).timeInMillis
    }

    init {
        LearnYourLangApplication.takeAuthorityToUseFirebaseAuthentication(this)
    }
}