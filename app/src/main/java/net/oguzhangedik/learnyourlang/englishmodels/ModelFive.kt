package net.oguzhangedik.learnyourlang.englishmodels

import net.oguzhangedik.learnyourlang.abstracts.Model

/**
 * Created by ordekci on 28.11.2017.
 */
class ModelFive : Model() {
    var englishWord: String = ""
    var optionImageUrlOne: String = ""
    var optionImageUrlTwo: String = ""
    var information: String = ""
    var trueOptionIndex : Long = 0
}