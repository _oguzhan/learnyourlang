package net.oguzhangedik.learnyourlang.englishmodels

import net.oguzhangedik.learnyourlang.abstracts.Model

/**
 * Created by ordekci on 28.11.2017.
 */
class ModelThree : Model() {
    var information: String = ""
    var sampleEnglishSentence: String = ""
    var sampleTurkishSentence: String = ""
}