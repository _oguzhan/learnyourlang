package net.oguzhangedik.learnyourlang.englishmodels

import net.oguzhangedik.learnyourlang.abstracts.Model

/**
 * Created by ordekci on 28.11.2017.
 */
class ModelTwo : Model() {
    var sampleEnglishSentence: String = ""
    var sampleTurkishSentence: String = ""
}