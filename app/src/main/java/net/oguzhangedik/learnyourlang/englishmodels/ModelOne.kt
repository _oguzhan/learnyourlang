package net.oguzhangedik.learnyourlang.englishmodels

import net.oguzhangedik.learnyourlang.abstracts.Model

/**
 * Created by ordekci on 28.11.2017.
 */
class ModelOne : Model() {
    var englishWord: String = ""
    var imageUrl: String = ""
    var turkishWord: String = ""
    var information: String = ""
    var sampleEnglishSentence: String = ""
    var sampleTurkishSentence: String = ""
}