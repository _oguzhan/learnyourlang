package net.oguzhangedik.learnyourlang.englishmodels

import net.oguzhangedik.learnyourlang.abstracts.Model

/**
 * Created by ordekci on 28.11.2017.
 */
class ModelFour : Model() {
    var imageUrl: String = ""
    var optionTextOne : String = ""
    var optionTextTwo : String = ""
    var information: String = ""
    var trueOptionIndex : Long = 0
}