package net.oguzhangedik.learnyourlang.firebase

import android.content.Context
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ProviderQueryResult
import net.oguzhangedik.learnyourlang.LearnYourLangApplication
import net.oguzhangedik.learnyourlang.interfaces.Authorization
import net.oguzhangedik.learnyourlang.utils.NetworkConnectionHelper

/**
 * Created by ordekci on 16.11.2017.
 */
class FirebaseAuthentication : Authorization.ApplicationContextAccess, Authorization.Firebase, Authorization.Register, Authorization.SignIn {

    private var mAuth: FirebaseAuth? = null
    private lateinit var applicationContext: Context

    override fun getApplicationContext(context: Context) {
        applicationContext = context
    }

    override fun getFirebaseAuth(mAuth: FirebaseAuth?) {
        this.mAuth = mAuth
    }

    override fun Authorization.Register.isUserRegistered(email: String, onComplete: ((task: Task<ProviderQueryResult>) -> Unit)?, throwConnectionError: (() -> Unit)?) {
        if (throwConnectionError != null && !NetworkConnectionHelper.isNetworkConnected(applicationContext)) {
            throwConnectionError()
            return
        }
        mAuth?.fetchProvidersForEmail(email)
                ?.addOnCompleteListener({
                    task ->
                    onComplete?.invoke(task)
                })
    }

    override fun Authorization.Register.register(email: String, password: String, onComplete: ((task: Task<AuthResult>) -> Unit)?, throwConnectionError: (() -> Unit)?) {
        if (throwConnectionError != null && !NetworkConnectionHelper.isNetworkConnected(applicationContext)) {
            throwConnectionError()
            return
        }
        mAuth?.createUserWithEmailAndPassword(email, password)
                ?.addOnCompleteListener({
                    task ->
                    onComplete?.invoke(task)
                })
    }

    override fun Authorization.SignIn.signIn(email: String, password: String, onComplete: ((task: Task<AuthResult>) -> Unit)?, throwConnectionError: (() -> Unit)?) {

        if (throwConnectionError != null && !NetworkConnectionHelper.isNetworkConnected(applicationContext)) {
            throwConnectionError()
            return
        }
        mAuth?.signInWithEmailAndPassword(email, password)
                ?.addOnCompleteListener({
                    task ->
                    onComplete?.invoke(task)
                })
    }

    init {
        LearnYourLangApplication.takeAuthorityToUseFirebaseAuthentication(this)
        LearnYourLangApplication.takeAuthorityToUseApplicationContext(this)
    }
}