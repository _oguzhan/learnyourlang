package net.oguzhangedik.learnyourlang.firebase

import android.content.Context
import android.graphics.Bitmap
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import net.oguzhangedik.learnyourlang.LearnYourLangApplication
import net.oguzhangedik.learnyourlang.interfaces.Authorization
import net.oguzhangedik.learnyourlang.utils.NetworkConnectionHelper
import net.oguzhangedik.learnyourlang.utils.RandomStringGenerator
import net.oguzhangedik.learnyourlang.utils.StringUtils
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.net.URLDecoder

/**
 * Created by ordekci on 12.12.2017.
 */
class CloudStorage : Authorization.ApplicationContextAccess, Authorization.Firebase, Authorization.FireStorage, Authorization.FireStorage.Upload, Authorization.FireStorage.Delete {
    override fun Authorization.FireStorage.Delete.deleteImageFromFirestorage(imageUrl: String, onComplete: ((task: Task<Void>) -> Unit)?, throwConnectionError: (() -> Unit)?) {
        if (throwConnectionError != null && !NetworkConnectionHelper.isNetworkConnected(applicationContext)) {
            throwConnectionError()
            return
        }
        val decodedUrl = URLDecoder.decode(imageUrl, "UTF-8")
        val childAndImageName: List<String> = StringUtils.between(FirebaseConstant.FireStorage.References.EnglishModelImages + "/", "?", decodedUrl).replace("?", "").split("/")
        val secondChild = childAndImageName[0]
        val imageName = childAndImageName[1]
        firebaseStorage.reference
                .child(FirebaseConstant.FireStorage.References.EnglishModelImages)
                .child(secondChild)//second child reference i, baska bir user in id si olabileceginden, url den aldik
                .child(imageName)
                .delete()
                .addOnCompleteListener { task -> onComplete?.invoke(task) }
    }

    lateinit var firebaseStorage: FirebaseStorage
    private lateinit var applicationContext: Context
    var mAuth: FirebaseAuth? = null

    override fun getApplicationContext(context: Context) {
        applicationContext = context
    }

    override fun getFirebaseAuth(mAuth: FirebaseAuth?) {
        this.mAuth = mAuth
    }


    override fun getFireStorage(firebaseStorage: FirebaseStorage) {
        this.firebaseStorage = firebaseStorage
    }


    override fun Authorization.FireStorage.Upload.uploadImage(bitmap: Bitmap, onFailure: ((exception: Exception) -> Unit)?, onSuccess: ((taskSnapshot: UploadTask.TaskSnapshot) -> Unit)?, throwConnectionError: (() -> Unit)?) {
        if (throwConnectionError != null && !NetworkConnectionHelper.isNetworkConnected(applicationContext)) {
            throwConnectionError()
            return
        }
        //Her user, image upload ederken second child reference si  kendi user id si olur.
        val imageRef = firebaseStorage
                .reference
                .child(FirebaseConstant.FireStorage.References.EnglishModelImages)
                .child(mAuth?.uid!!)//second child reference
                .child("${RandomStringGenerator.getOne()}.jpg")

        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        imageRef
                .putBytes(data)
                .addOnFailureListener { exception: Exception -> onFailure?.invoke(exception) }
                .addOnSuccessListener { taskSnapshot: UploadTask.TaskSnapshot? -> onSuccess?.invoke(taskSnapshot!!) }
    }

    init {
        LearnYourLangApplication.takeAuthorityToUseApplicationContext(this)
        LearnYourLangApplication.takeAuthorityToUseFirebaseStorage(this)
        LearnYourLangApplication.takeAuthorityToUseFirebaseAuthentication(this)
    }
}