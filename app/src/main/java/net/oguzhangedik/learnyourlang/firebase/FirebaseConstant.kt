package net.oguzhangedik.learnyourlang.firebase

/**
 * Created by admin on 27.11.2017.
 */
class FirebaseConstant {
    class FireStore {
        class Collections {
            companion object {
                val Profiles = "Profiles"
                val EnglishModels = "EnglishModels"
            }
        }
    }

    class FireStorage {
        class References {
            companion object {
                val EnglishModelImages = "englishmodelimages"
            }
        }
    }
}