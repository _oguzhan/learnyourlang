package net.oguzhangedik.learnyourlang.firebase

import android.content.Context
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import net.oguzhangedik.learnyourlang.LearnYourLangApplication
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.interfaces.Authorization
import net.oguzhangedik.learnyourlang.utils.NetworkConnectionHelper

/**
 * Created by ordekci on 8.12.2017.
 */
class CloudFireStore : Authorization.ApplicationContextAccess, Authorization.FireStore, Authorization.FireStore.Get, Authorization.FireStore.Index, Authorization.FireStore.Update {
    private lateinit var applicationContext: Context

    override fun Authorization.FireStore.Update.updateModel(collection: String, model: Model, onComplete: ((task: Task<Void>) -> Unit)?, throwConnectionError: (() -> Unit)?) {
        if (throwConnectionError != null && !NetworkConnectionHelper.isNetworkConnected(applicationContext)) {
            throwConnectionError()
            return
        }
        model.updateMillis()
        val mappedObject = ObjectMapper().convertValue(model, Map::class.java)
        fireStore.collection(collection)
                .document(model.id)
                .update(mappedObject as MutableMap<String, Any>)
                .addOnCompleteListener { task -> onComplete?.invoke(task) }
    }

    override fun Authorization.FireStore.Index.addNewModel(collection: String, model: Model, onComplete: ((task: Task<DocumentReference>) -> Unit)?, throwConnectionError: (() -> Unit)?) {
        if (throwConnectionError != null && !NetworkConnectionHelper.isNetworkConnected(applicationContext)) {
            throwConnectionError()
            return
        }
        fireStore
                .collection(collection)
                .add(model)
                .addOnCompleteListener { task -> onComplete?.invoke(task) }
    }

    override fun getApplicationContext(context: Context) {
        applicationContext = context
    }

    private lateinit var fireStore: FirebaseFirestore

    override fun Authorization.FireStore.Get.getAll(collection: String, onComplete: ((task: Task<QuerySnapshot>) -> Unit)?, throwConnectionError: (() -> Unit)?) {
        if (throwConnectionError != null && !NetworkConnectionHelper.isNetworkConnected(applicationContext)) {
            throwConnectionError()
            return
        }
        fireStore.
                collection(collection)
                .get()
                .addOnCompleteListener { task -> onComplete?.invoke(task) }
    }

    override fun getFireStore(fireStore: FirebaseFirestore) {
        this.fireStore = fireStore
    }

    init {
        LearnYourLangApplication.takeAuthorityToUseApplicationContext(this)
        LearnYourLangApplication.takeAuthorityToUseFirebaseFirestore(this)
    }
}