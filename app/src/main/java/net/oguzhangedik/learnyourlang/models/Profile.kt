package net.oguzhangedik.learnyourlang.models

/**
 * Created by ordekci on 27.11.2017.
 */
data class Profile(var userId : String, var name : String, var surname : String, var totalPoint : Int)
