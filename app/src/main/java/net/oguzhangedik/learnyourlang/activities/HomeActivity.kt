package net.oguzhangedik.learnyourlang.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar_home.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.abstracts.DrawableClickListener
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.adapters.ReadableModelPagerAdapter
import net.oguzhangedik.learnyourlang.englishmodels.*
import net.oguzhangedik.learnyourlang.firebase.CloudFireStore
import net.oguzhangedik.learnyourlang.firebase.FirebaseConstant
import net.oguzhangedik.learnyourlang.interfaces.Authorization
import net.oguzhangedik.learnyourlang.utils.Constants
import net.oguzhangedik.learnyourlang.utils.KeyboardUtil
import net.oguzhangedik.learnyourlang.utils.Reflector
import org.jetbrains.anko.toast
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by ordekci on 13.11.2017.
 */
class HomeActivity : BaseAppCompatActivity(), SwipeRefreshLayout.OnRefreshListener, Authorization.FireStore.Get {
    private var menuSearch: MenuItem? = null

    override fun onRefresh() {
        showRefreshingIndicator(true)
        getAll(FirebaseConstant.FireStore.Collections.EnglishModels)
    }

    private fun showRefreshingIndicator(state: Boolean) =
            swipeLayout.post { swipeLayout.isRefreshing = state }


    private lateinit var readableModelPagerAdapter: ReadableModelPagerAdapter

    override fun Authorization.FireStore.Get.getAll(collection: String, ignored0: ((ignored1: Task<QuerySnapshot>) -> Unit)?, ignored2: (() -> Unit)?) {
        fun fillSearchViewList(searchViewList: ArrayList<String>, model: Model) {
            when (model) {
                is ModelOne -> {
                    searchViewList.add(model.englishWord + " -- " + model.turkishWord)

                    val engWordSize = model.sampleEnglishSentence.trim().split(" ").size
                    if (engWordSize > 1) searchViewList.add(model.sampleEnglishSentence)

                    val turWordSize = model.sampleTurkishSentence.trim().split(" ").size
                    if (turWordSize > 1) searchViewList.add(model.sampleTurkishSentence)

                }
                is ModelTwo -> {
                    searchViewList.add(model.sampleEnglishSentence + " -- " + model.sampleTurkishSentence)
                }
                is ModelThree -> {
                    searchViewList.add(model.information)

                    if (model.sampleEnglishSentence.trim().isNotEmpty()) {
                        val engWordSize = model.sampleEnglishSentence.trim().split(" ").size
                        if (engWordSize > 1) searchViewList.add(model.sampleEnglishSentence)
                    }

                    if (model.sampleTurkishSentence.trim().isNotEmpty()) {
                        val turWordSize = model.sampleTurkishSentence.trim().split(" ").size
                        if (turWordSize > 1) searchViewList.add(model.sampleTurkishSentence)
                    }
                }

                //ModelFour is unnecessary to add search bar as structural

                is ModelFive -> {
                    searchViewList.add(model.information)
                }
            }
        }

        fun onComplete(task: Task<QuerySnapshot>) {
            showProgress(false)
            showRefreshingIndicator(false)
            if (task.isSuccessful) {

                val models = ArrayList<Model>()
                val searchViewList = ArrayList<String>()
                task.result.forEach {
                    val model = Reflector().convertToModel(packageName, it.data as HashMap<String, Any>)
                    model.id = it.id
                    models.add(model)
                    fillSearchViewList(searchViewList, model)
                }
                //listedeki itemlarin yerlerini degistirmek icin
                Collections.shuffle(models)

                readableModelPagerAdapter = ReadableModelPagerAdapter(supportFragmentManager, models)
                modelPager.adapter = readableModelPagerAdapter

                //Ref : https://stackoverflow.com/a/21182177/3341089
                val searchBarAdapter = ArrayAdapter<String>(this@HomeActivity,
                        android.R.layout.two_line_list_item,android.R.id.text1, searchViewList)
                actvSearch.setAdapter(searchBarAdapter)
            } else {
                toast("Veriler getirilirken bir hata olustu. " + task.exception.toString())
            }
        }

        CloudFireStore().getAll(collection, { task -> onComplete(task) }) {
            showProgress(false)
            showRefreshingIndicator(false)
            toast("Check your network connection")
        }
    }

    override fun getLayoutResourceId(): Int? {
        return R.layout.activity_home
    }

    override fun getToolBar(): Toolbar {
        return toolBar
    }

    override fun getContainerView(): ViewGroup {
        return containerView
    }

    override fun getProgressBar(): ProgressBar {
        return progressBar
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showProgress(true)
        getAll(FirebaseConstant.FireStore.Collections.EnglishModels)
    }

    override fun placeListeners() {
        super.placeListeners()
        swipeLayout.setOnRefreshListener(this)
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light)

        /* Ref : https://stackoverflow.com/a/29946734/3341089
         * After adding SwipeRefreshLayout to refresh modelPager, modelPager left - right swiping
         * between pages the scrolling is too sensitive. A little swipe down will trigger the
         * SwipeRefreshLayout refresh too.
         */
        modelPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, v: Float, i1: Int) {}

            override fun onPageSelected(position: Int) {}

            override fun onPageScrollStateChanged(state: Int) {
                if (swipeLayout != null) swipeLayout.isEnabled = state == ViewPager.SCROLL_STATE_IDLE
            }
        })

        actvSearch.setOnTouchListener(object : DrawableClickListener.RightDrawableClickListener(actvSearch) {
            override fun onDrawableClick(): Boolean {
                if (actvSearch.text.isNotEmpty()) {
                    actvSearch.setText(Constants.EMPTY_STRING)
                } else setSearchBarVisibility()
                return true
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)
        menuSearch = menu?.findItem(R.id.menu_search)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_profile -> startActivity(ProfileActivity.newInstance(this))
            R.id.menu_new_model_adding -> startActivity(ModelAddingActivity.newInstance(this))
            R.id.menu_update_selected_model -> {
                val intent = UpdateWritableModelActivity.newInstance(this)
                intent.putExtra(Constants.BundleKeys.MODEL, readableModelPagerAdapter.getCurrentModel(modelPager.currentItem))
                startActivity(intent)
            }
            R.id.menu_search -> {
                menuSearch?.isVisible = false; actvSearch.visibility = View.VISIBLE; }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!setSearchBarVisibility()!!) return

        super.onBackPressed()
    }

    private fun setSearchBarVisibility(): Boolean? {
        val visibility = menuSearch?.isVisible!!
        if (!visibility) {
            menuSearch?.isVisible = true
            actvSearch.visibility = View.GONE
            KeyboardUtil.hideKeyboard(this)
        }
        return visibility
    }

    companion object {
        fun newInstance(context: Context): Intent {
            return Intent(context, HomeActivity::class.java)
        }
    }
}