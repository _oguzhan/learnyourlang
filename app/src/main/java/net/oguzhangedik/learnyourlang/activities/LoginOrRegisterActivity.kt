package net.oguzhangedik.learnyourlang.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ProgressBar
import android.widget.TextView
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.ProviderQueryResult
import kotlinx.android.synthetic.main.activity_login_or_register.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.firebase.FirebaseAuthentication
import net.oguzhangedik.learnyourlang.interfaces.Authorization
import net.oguzhangedik.learnyourlang.utils.EmailValidator
import net.oguzhangedik.learnyourlang.utils.PasswordValidator
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

class LoginOrRegisterActivity : BaseAppCompatActivity(), Authorization.Register, Authorization.SignIn {

    override fun getLayoutResourceId(): Int? {
        return null
    }

    private lateinit var tempPassHolder: String

    override fun getToolBar(): Toolbar {
        return toolBar
    }

    override fun setupToolBar() {
        super.setupToolBar()
    }

    override fun getContainerView(): ViewGroup {
        return login_form
    }

    override fun getProgressBar(): ProgressBar {
        return progressBar
    }

    override fun placeListeners() {
        super.placeListeners()
        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })
        btnContinue.setOnClickListener { attemptLogin() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getFirebaseAuth()?.currentUser != null) redirectHomePage()
        else setContentView(R.layout.activity_login_or_register)
    }

    private fun attemptLogin() {
        listOf(email, password).map { it.error = null }

        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()

        // Check for a valid password, if the user entered one.
        val checkResult = PasswordValidator.check(passwordStr)

        val (cancel, focusView) = if (checkResult != null) {
            password.error = checkResult
            true to password
        } else if (TextUtils.isEmpty(emailStr)) {
            email.error = getString(R.string.error_field_required)
            true to email
        } else if (!EmailValidator.isEmailValid(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            true to email
        } else false to null

        if (cancel) focusView?.requestFocus()
        else {
            tempPassHolder = passwordStr
            isUserRegistered(emailStr)
        }
    }

    override fun Authorization.SignIn.signIn(email: String, password: String, ignore0: ((ignore1: Task<AuthResult>) -> Unit)?, ignore2: (() -> Unit)?) {
        showProgress(true)

        fun onComplete(task: Task<AuthResult>) {
            showProgress(false)
            if (task.isSuccessful) redirectHomePage()
            else alert("Giris yapilamadi lutfen bilgilerinizi kontrol ediniz.", "Uyari") {
                positiveButton("OK") {}
            }.show()
        }

        FirebaseAuthentication().signIn(email, password, { task -> onComplete(task) }) {
            showProgress(false)
            toast("Check your network connection")
        }
    }

    override fun Authorization.Register.register(email: String, password: String, ignore0: ((ignore1: Task<AuthResult>) -> Unit)?, ignore2: (() -> Unit)?) {
        showProgress(true)

        fun onComplete(task: Task<AuthResult>) {
            showProgress(false)
            if (task.isSuccessful) {
                showProgress(true)

                this@LoginOrRegisterActivity.signIn(email, password, null)

            } else alert("Kayit isleminde bir hata olustu lutfen tekrar deneyin " + task.exception, "Uyari") {
                positiveButton("OK") {}
            }.show()
        }

        FirebaseAuthentication().register(email, password, { task -> onComplete(task) }) {
            showProgress(false)
            toast("Check your network connection")
        }
    }

    override fun Authorization.Register.isUserRegistered(email: String, ignore0: ((ignore1: Task<ProviderQueryResult>) -> Unit)?, ignore2: (() -> Unit)?) {
        showProgress(true)

        fun onComplete(task: Task<ProviderQueryResult>) {
            showProgress(false)
            if (task.isSuccessful) {
                val size: Int = task.result.providers?.size!!
                alert(if (size > 0) "Bu mail adresi sisteme zaten kayitli" else "Yeni kullanici kaydi yapilacak", "Uyari") {
                    positiveButton("OK") {
                        if (size > 0) signIn(email, tempPassHolder)
                        else register(email, tempPassHolder)
                        tempPassHolder = ""
                    }
                }.show()
            } else {
                alert("Firebase de bir hata olustu : " + task.exception, "Uyari") {
                    positiveButton("OK") {}
                }.show()
            }
        }

        FirebaseAuthentication().isUserRegistered(email, { task -> onComplete(task) }) {
            showProgress(false)
            toast("Check your network connection")
        }
    }

    fun redirectHomePage() {
        finish()
        startActivity(HomeActivity.newInstance(this))
    }

    companion object {
        fun newInstance(context: Context): Intent {
            return Intent(context, LoginOrRegisterActivity::class.java)
        }
    }
}
