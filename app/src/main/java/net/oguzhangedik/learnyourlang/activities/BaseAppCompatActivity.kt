package net.oguzhangedik.learnyourlang.activities

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.google.firebase.auth.FirebaseAuth
import net.oguzhangedik.learnyourlang.LearnYourLangApplication
import net.oguzhangedik.learnyourlang.interfaces.Authorization
import net.oguzhangedik.learnyourlang.interfaces.ILayoutResource
import net.oguzhangedik.learnyourlang.interfaces.IPlaceListeners
import net.oguzhangedik.learnyourlang.interfaces.IProgressAccess

/**
 * Created by ordekci on 13.11.2017.
 */
abstract class BaseAppCompatActivity : AppCompatActivity(), Authorization.Firebase, ILayoutResource, IProgressAccess, IPlaceListeners {

    private var mAuth: FirebaseAuth? = null

    override fun getFirebaseAuth(mAuth: FirebaseAuth?) {
        this.mAuth = mAuth
    }

    fun getFirebaseAuth(): FirebaseAuth? {
        return mAuth
    }

    init {
        LearnYourLangApplication.takeAuthorityToUseFirebaseAuthentication(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkUserState()
        if (getLayoutResourceId() != null)
            setContentView(getLayoutResourceId()!!)
    }

    override fun onResume() {
        super.onResume()
        checkUserState()
    }

    private fun checkUserState() {
        //https://stackoverflow.com/a/45977825/3341089
        val me = this as Any
        if (getFirebaseAuth()?.currentUser == null && me !is LoginOrRegisterActivity) redirectLoginPage()
    }

    fun redirectLoginPage() {
        getFirebaseAuth()?.signOut()
        finish()
        val intent = LoginOrRegisterActivity.newInstance(this)
        //https://stackoverflow.com/a/39484617/3341089
        finishAffinity()
        startActivity(intent)
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        placeListeners()
        setupToolBar()
    }

    override fun showProgress(show: Boolean) {
        fun callProgress(): Unit {
            // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
            // for very easy animations. If available, use these APIs to fade-in
            // the progress spinner.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

                getContainerView().visibility = if (show) View.GONE else View.VISIBLE
                getContainerView().animate()
                        .setDuration(shortAnimTime)
                        .alpha((if (show) 0 else 1).toFloat())
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                getContainerView().visibility = if (show) View.GONE else View.VISIBLE
                            }
                        })

                getProgressBar().visibility = if (show) View.VISIBLE else View.GONE
                getProgressBar().animate()
                        .setDuration(shortAnimTime)
                        .alpha((if (show) 1 else 0).toFloat())
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                getProgressBar().visibility = if (show) View.VISIBLE else View.GONE
                            }
                        })
            } else {
                // The ViewPropertyAnimator APIs are not available, so simply show
                // and hide the relevant UI components.
                getProgressBar().visibility = if (show) View.VISIBLE else View.GONE
                getContainerView().visibility = if (show) View.GONE else View.VISIBLE
            }
        }
        Handler().post({
            kotlin.run {
                callProgress()
            }
        })
    }

    open fun setupToolBar() {
        getToolBar().title = resources.getString(packageManager.getActivityInfo(componentName, 0).descriptionRes)
        setSupportActionBar(getToolBar())
    }

    override fun placeListeners()  = Unit

    abstract fun getProgressBar(): ProgressBar

    abstract fun getContainerView(): ViewGroup

    abstract fun getToolBar(): Toolbar
}