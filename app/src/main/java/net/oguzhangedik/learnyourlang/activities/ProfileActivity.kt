package net.oguzhangedik.learnyourlang.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.ProgressBar
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import net.oguzhangedik.learnyourlang.LearnYourLangApplication
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.firebase.FirebaseConstant
import net.oguzhangedik.learnyourlang.interfaces.Authorization
import net.oguzhangedik.learnyourlang.models.Profile
import org.jetbrains.anko.toast

/**
 * Created by ordekci on 14.11.2017.
 */
class ProfileActivity : BaseAppCompatActivity(), Authorization.FireStore {
    private lateinit var fireStore: FirebaseFirestore

    override fun getLayoutResourceId(): Int? {
        return R.layout.activity_profile
    }

    override fun getFireStore(fireStore: FirebaseFirestore) {
        this.fireStore = fireStore
    }

    override fun placeListeners() {
        super.placeListeners()
        toolBar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun setupToolBar() {
        super.setupToolBar()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun getProgressBar(): ProgressBar {
        return progressBar
    }

    override fun getContainerView(): ViewGroup {
        return containerView
    }

    override fun getToolBar(): Toolbar {
        return toolBar
    }

    init {
        LearnYourLangApplication.takeAuthorityToUseFirebaseFirestore(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    /**
     * Bu kod, eger kullanici profil bilgilerini girmediyse, edittext, girdiyse textview seklinde gorunsun diye kullanicaz.
     */
    private fun addProfileIfNotExist() {
        val profile = Profile(getFirebaseAuth()?.currentUser?.uid!!, "Oguzhan", "Gedik", 0)

        val profileCollection = fireStore.collection(FirebaseConstant.FireStore.Collections.Profiles)

        profileCollection.whereEqualTo("userId", profile.userId).get().addOnCompleteListener {
            task ->
            if (task.isSuccessful) {
                if (!task.result?.isEmpty!!) toast("Zaten ekli")
                else profileCollection.add(profile).addOnCompleteListener {
                    task2 ->
                    if (task2.isSuccessful) toast("Profil eklendi") else toast("Profil eklenemedi" + task2.exception.toString())
                }
            }
        }
    }

    private fun addNewModel(model: Model) {
        val englishModelsCollection = fireStore.collection(FirebaseConstant.FireStore.Collections.EnglishModels)
        englishModelsCollection.add(model).addOnCompleteListener {
            task ->
            if (task.isSuccessful) toast("${model._type} eklendi") else toast("${model._type}  eklenemedi " + task.exception.toString())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.profile_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (R.id.menu_logout == item?.itemId) {
            redirectLoginPage()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun newInstance(context: Context): Intent {
            return Intent(context, ProfileActivity::class.java)
        }
    }
}