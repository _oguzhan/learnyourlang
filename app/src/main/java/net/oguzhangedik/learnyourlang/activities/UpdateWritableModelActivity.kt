package net.oguzhangedik.learnyourlang.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.activity_update_writable_model.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.abstracts.Model
import net.oguzhangedik.learnyourlang.englishmodels.*
import net.oguzhangedik.learnyourlang.fragments.modelfragments.ModelWritableFragment
import net.oguzhangedik.learnyourlang.fragments.modelfragments.writable.*
import net.oguzhangedik.learnyourlang.utils.Constants
import net.oguzhangedik.learnyourlang.utils.KeyboardUtil

/**
 * Created by ordekci on 18.12.2017.
 */
class UpdateWritableModelActivity : BaseAppCompatActivity() {
    lateinit var modelWritableFragment: ModelWritableFragment

    override fun getLayoutResourceId(): Int? {
        return R.layout.activity_update_writable_model
    }

    override fun getProgressBar(): ProgressBar {
        return progressBar
    }

    override fun getContainerView(): ViewGroup {
        return containerView
    }

    override fun getToolBar(): Toolbar {
        return toolBar
    }

    override fun setupToolBar() {
        super.setupToolBar()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.model_updating_menu, menu)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val model = intent.extras.getSerializable(Constants.BundleKeys.MODEL) as Model

        modelWritableFragment =
                when (model) {
                    is ModelOne -> ModelOneWritableFragment()
                    is ModelTwo -> ModelTwoWritableFragment()
                    is ModelThree -> ModelThreeWritableFragment()
                    is ModelFour -> ModelFourWritableFragment()
                    is ModelFive -> ModelFiveWritableFragment()
                    else -> throw UnsupportedOperationException("You should add new ModelWritableFragment to update incoming new model")
                }
        val bundle = Bundle()
        bundle.putSerializable(Constants.BundleKeys.MODEL, model)
        modelWritableFragment.arguments = bundle
        supportFragmentManager.beginTransaction().replace(R.id.content_frame, modelWritableFragment)
                .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (R.id.update_menu == item?.itemId) {
            KeyboardUtil.hideKeyboard(this)
            modelWritableFragment.update()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun newInstance(context: Context): Intent {
            return Intent(context, UpdateWritableModelActivity::class.java)
        }
    }
}