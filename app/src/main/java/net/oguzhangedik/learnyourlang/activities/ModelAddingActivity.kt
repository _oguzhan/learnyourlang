package net.oguzhangedik.learnyourlang.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.activity_model_adding.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import net.oguzhangedik.learnyourlang.R
import net.oguzhangedik.learnyourlang.adapters.WritableModelPagerAdapter
import net.oguzhangedik.learnyourlang.utils.KeyboardUtil

/**
 * Created by ordekci on 8.12.2017.
 */
class ModelAddingActivity : BaseAppCompatActivity() {
    lateinit var writableModelPagerAdapter: WritableModelPagerAdapter

    override fun getProgressBar(): ProgressBar {
        return progressBar
    }

    override fun getContainerView(): ViewGroup {
        return containerView
    }

    override fun getToolBar(): Toolbar {
        return toolBar
    }

    override fun setupToolBar() {
        super.setupToolBar()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun getLayoutResourceId(): Int? {
        return R.layout.activity_model_adding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        writableModelPagerAdapter = WritableModelPagerAdapter(supportFragmentManager)
        /* Normalde, mesela ModelFourWritableFragment a galeriden ekledigimiz bi foto, view pager i
           swipe ederek itemlari arasinda dolasirken tekrar ModelFourWritableFragment a gelince, imageView a
           yukledigimiz resim kayboluyordu. offscreenPageLimit e asagidaki view pagerin item sayisini verince
           bu problem cozulmus oldu
           ref : https://stackoverflow.com/a/42969016/3341089  */
        writableModelPager.offscreenPageLimit = writableModelPagerAdapter.writableFragments.size
        writableModelPager.adapter = writableModelPagerAdapter
    }

    companion object {
        fun newInstance(context: Context): Intent {
            return Intent(context, ModelAddingActivity::class.java)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.model_adding_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (R.id.save_menu == item?.itemId) {
            KeyboardUtil.hideKeyboard(this)
            writableModelPagerAdapter.getCurrentFragment(writableModelPager.currentItem).save()
        }
        return super.onOptionsItemSelected(item)
    }
}