package net.oguzhangedik.learnyourlang.utils

import android.Manifest
import android.content.pm.PackageManager
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat

/**
 * Created by ordekci on 20.12.2017.
 */
object PermissionUtil {
    //Ref : https://stackoverflow.com/a/33080682/3341089
    fun checkCamera(fragment: Fragment): Boolean {
        if (ContextCompat.checkSelfPermission(fragment.activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            fragment.requestPermissions(arrayOf(Manifest.permission.CAMERA), Constants.RequestCode.PERMISSION_REQUEST_CAMERA)
            return false
        }
        return true
    }
}