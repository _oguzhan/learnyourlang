package net.oguzhangedik.learnyourlang.utils

import java.util.*

/**
 * Created by ordekci on 12.12.2017.
 */
object RandomStringGenerator {
    fun getOne(): String = UUID.randomUUID().toString().replace("-", "")
}