package net.oguzhangedik.learnyourlang.utils

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by ordekci on 16.12.2017.
 */
@GlideModule
class MyAppGlideModule : AppGlideModule()