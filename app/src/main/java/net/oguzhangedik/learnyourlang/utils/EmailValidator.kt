package net.oguzhangedik.learnyourlang.utils

/**
 * Created by oguz on 10.11.2017.
 */
object EmailValidator {
    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}