package net.oguzhangedik.learnyourlang.utils

/**
 * Created by ordekci on 7.12.2017.
 */
object Constants {
    val EMPTY_STRING = ""

    object BundleKeys {
        val MODEL = "MODEL"
    }

    object RequestCode {
        val PICK_IMAGE = 1
        val CAMERA_REQUEST = 2
        val PERMISSION_REQUEST_CAMERA = 4
    }

    object Tag {
        val IMAGE_UPDATED = "IMAGE_UPDATED"
    }
}