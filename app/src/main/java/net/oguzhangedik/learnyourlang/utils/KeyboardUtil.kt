package net.oguzhangedik.learnyourlang.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * Created by ordekci on 10.01.2018.
 */
object KeyboardUtil {
    fun hideKeyboard(activity: Activity) {
        var view = activity.currentFocus

        /* Ref : https://stackoverflow.com/questions/42126247/getcurrentfocus-returns-null
         * If view is null */
        if(view == null) view = activity.findViewById<View>(android.R.id.content)

        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}