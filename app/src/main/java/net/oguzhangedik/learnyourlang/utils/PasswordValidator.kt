package net.oguzhangedik.learnyourlang.utils

import java.util.regex.Pattern

/**
 * Created by oguz on 9.11.2017.
 */
object PasswordValidator {
    fun check(password: String): String? {
        /*
          (?=.*[@\$%&#_()=+?»«<>£§€{}.[\]-]) -> must have at least 1 special character
          (?=.*[A-Z])   -> Must have at least 1 upper case letter
          (?=.*[a-z])   -> Must have at least 1 lower case letter
          (?=.*\\d)     -> Must have at least 1 digit
          (?<=.{4,})$") -> Must be equal or superior to 4 chars.
        */
        var validationMessages = arrayOf("must have at least 1 special character"
                ,"Must have at least 1 upper case letter"
                ,"Must have at least 1 lower case letter"
                ,"Must have at least 1 digit"
                ,"Must be equal or superior to 4 chars")
        //"^(?=.*[@$%&#_()=+?»«<>£§€{}\\[\\]-])(?=.*[A-Z])(?=.*[a-z])(?=.*\\d).*(?<=.{4,})$"
        val pattern = Pattern.compile(".{4,}")
        return if (pattern.matcher(password).matches()) null else validationMessages[4]
    }
}