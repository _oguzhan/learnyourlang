package net.oguzhangedik.learnyourlang.utils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by admin on 13.11.2017.
 */
object NetworkConnectionHelper {
    //http://www.andevcon.com/news/introduction-to-networking-using-kotlin
    fun isNetworkConnected(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager// 1
        val networkInfo = connectivityManager.activeNetworkInfo // 2
        return networkInfo != null && networkInfo.isConnected // 3
    }
}