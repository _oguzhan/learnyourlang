package net.oguzhangedik.learnyourlang.utils

import android.content.Intent
import android.provider.MediaStore
import android.support.v4.app.Fragment

/**
 * Created by admin on 20.12.2017.
 */
object CameraImageSelector {
    fun captureImage(fragment: Fragment, requestCode: Int) {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        fragment.startActivityForResult(cameraIntent, requestCode)
    }
}