package net.oguzhangedik.learnyourlang.utils

import net.oguzhangedik.learnyourlang.abstracts.Model

/**
 * Created by ordekci on 5.12.2017.
 */
class Reflector {
    /**
     * https://stackoverflow.com/questions/7495785/java-how-to-instantiate-a-class-from-string
     * https://stackoverflow.com/questions/12784848/how-to-access-a-fields-value-in-an-object-using-reflection
     * https://stackoverflow.com/questions/39589780/kotlin-reflection-getting-all-field-names-of-a-class
     * https://discuss.kotlinlang.org/t/generic-object-creation/1663
     * https://stackoverflow.com/questions/6094575/creating-an-instance-using-the-class-name-and-calling-constructor
     * https://stackoverflow.com/questions/6835752/how-do-i-cast-a-hashmap-to-a-concrete-class
     */
    fun convertToModel(packageName: String, hashMap: HashMap<String, Any>): Model {

        val aclass = Class.forName(packageName + ".englishmodels." + hashMap["_type"])

        val generatedClass = aclass.newInstance() as Model

        val fields = generatedClass::class.java.declaredFields

        fields.forEach {
            it.isAccessible = true
            if(hashMap[it.name] != null)
            it.set(generatedClass, hashMap[it.name])
        }

        return generatedClass
    }
}