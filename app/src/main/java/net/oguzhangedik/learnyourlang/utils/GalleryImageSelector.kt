package net.oguzhangedik.learnyourlang.utils

import android.content.Intent
import android.provider.MediaStore
import android.support.v4.app.Fragment

/**
 * Created by ordekci on 11.12.2017.
 */
object GalleryImageSelector {
    /**
     * ref : https://stackoverflow.com/a/36929403/3341089
     */
    fun pickImage(fragment: Fragment, requestCode: Int) {
        val intent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        intent.type = "image/*"
        intent.putExtra("crop", "true")
        intent.putExtra("scale", true)
        /* Asagida yorum satirina alinan kisimlar kaldirildiginda, image cropper in regtangle alani serbest
           kalmis oldu, boylelikle artik kare seklind degil, kullanici istedigi rahatlikta image cropping
           yapabilecek */
/*        intent.putExtra("outputX", 512)
        intent.putExtra("outputY", 512)
        intent.putExtra("aspectX", 1)
        intent.putExtra("aspectY", 1)*/
        intent.putExtra("return-data", true)
        fragment.startActivityForResult(intent, requestCode)
    }
}