package net.oguzhangedik.learnyourlang.utils

/**
 * Created by ordekci on 19.12.2017.
 */
object StringUtils {
    //Ref : https://stackoverflow.com/a/40405238/3341089
    fun between(start: String, end: String, input: String): String {
        val startIndex = input.indexOf(start)
        val endIndex = input.lastIndexOf(end)

        return if (startIndex == -1 || endIndex == -1) input
        else input.substring(startIndex + start.length, endIndex + end.length).trim()
    }
}